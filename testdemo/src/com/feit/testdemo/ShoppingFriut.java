package com.feit.testdemo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class ShoppingFriut {

	private static Map map = new HashMap<String, BigDecimal>();
	public static void main(String[] args) {
		/*
		 * MarketSell marketSell = new AppleSell(); FruitPrice apple =
		 * marketSell.sell(true, DisCount.SIX.getDisnub(), "2"); BigDecimal appres =
		 * apple.price();
		 * 
		 * MarketSell marketSell2 = new StraberrySell(); FruitPrice sFruitPrice =
		 * marketSell2.sell(false, DisCount.TEN.getDisnub(),"3"); BigDecimal stres2 =
		 * sFruitPrice.price();
		 * 
		 * MarketSell mangoSell = new MangoSell(); FruitPrice mangoPrice =
		 * mangoSell.sell(false, DisCount.TEN.getDisnub(),"1"); BigDecimal mango =
		 * mangoPrice.price();
		 * 
		 * BigDecimal totalPrice = appres.add(stres2).add(mango); int flag =
		 * totalPrice.compareTo(new BigDecimal(FullRedtion.ONE.getMessage())); if(flag>0
		 * ) { totalPrice = totalPrice.subtract(new
		 * BigDecimal(FullRedtion.ONE.getCode())); }
		 * 
		 * System.out.println(totalPrice);
		 */
		
		Map  fs = shoppingTest();
		Custmer cs = (Custmer) fs.get("custmer");
		String name = cs.getName();
		Map info = (Map) fs.get("fruitInfo");
		Set set =info.keySet();
		StringBuffer sb = new StringBuffer();
		sb.append("顾客 :").append(name).append("   购买水果 :").append("\r\n");
		for (Object object : set) {
			if(object.toString().equalsIgnoreCase(Fruit.APPLE.getMessage())) {
				MarketSell m = new AppleSell();
				FruitPrice a = m.sell(Constant.DISCOUNT_FLAG_Y, DisCount.SIX.getDisnub(), info.get(object).toString());
				BigDecimal ap= a.price();
				sb.append(Fruit.APPLE.getMessage()).append(info.get(object).toString()).append(" 斤").append(ap).append(" 元 \r\n");
				if(map.get("total")== null) {
					map.put("total",ap );
				}else {
				    BigDecimal rest=ap.add((BigDecimal) map.get("total"));
				    map.put("total",rest );
				};
				
			}
			if(object.toString().equalsIgnoreCase(Fruit.STRAWBERRY.getMessage())) {
				MarketSell m = new StraberrySell();
				FruitPrice a = m.sell(Constant.DISCOUNT_FLAG_N, DisCount.TEN.getDisnub(), info.get(object).toString());
				BigDecimal ap= a.price();
				sb.append(Fruit.STRAWBERRY.getMessage()).append(info.get(object).toString()).append(" 斤").append(ap).append(" 元 \r\n");
				
				if(map.get("total")== null) {
					map.put("total",ap );
				}else {
				    BigDecimal rest=ap.add((BigDecimal) map.get("total"));
				    map.put("total",rest );
				};
			}
			if(object.toString().equalsIgnoreCase(Fruit.MANGO.getMessage())) {
				MarketSell m = new MangoSell();
				FruitPrice a = m.sell(Constant.DISCOUNT_FLAG_N, DisCount.TEN.getDisnub(), info.get(object).toString());
				BigDecimal ap= a.price();
				sb.append(Fruit.MANGO.getMessage()).append(info.get(object).toString()).append(" 斤").append(ap).append(" 元 \r\n");
				
				if(map.get("total")== null) {
					map.put("total",ap );
				}else {
				    BigDecimal rest=ap.add((BigDecimal) map.get("total"));
				    map.put("total",rest );
				};
			
			}
		}
		
		BigDecimal totalPrice = (BigDecimal) map.get("total");
		sb.append("原价 ：").append(totalPrice).append("元").append("满减");
		 int flag = totalPrice.compareTo(new BigDecimal(FullRedtion.ONE.getMessage())); 
		 if(flag>0) { 
			 sb.append(FullRedtion.ONE.getCode()).append(" 元 \r\n");
			 totalPrice = totalPrice.subtract(new BigDecimal(FullRedtion.ONE.getCode())); 
		}
		sb.append("总价 ：").append(totalPrice).append("元");
		System.out.println("info" +"---"+sb.toString());
		
		
		
	}
	
	public static Map shoppingTest() {
		Custmer custmer = new Custmer();
		custmer.setName("A");
		Map cusInfo = new HashMap<String, Object>();
		Map fruitInfo = new HashMap<String, Object>();
		//List flist = new ArrayList<>();
		
		fruitInfo.put(Fruit.STRAWBERRY.getMessage(), "3");
		fruitInfo.put(Fruit.APPLE.getMessage(), "3");
		fruitInfo.put(Fruit.MANGO.getMessage(), "3");
		cusInfo.put("custmer", custmer);
		cusInfo.put("fruitInfo", fruitInfo);
		return cusInfo;
	};
	
}
