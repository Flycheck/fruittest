package com.feit.testdemo;

import java.math.BigDecimal;

public interface FruitPrice {

	BigDecimal price();
}
