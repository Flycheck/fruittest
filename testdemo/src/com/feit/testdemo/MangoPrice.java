package com.feit.testdemo;

import java.math.BigDecimal;

public class MangoPrice implements FruitPrice {
	
	private String disnum;
	private String number;
	private boolean flag;

	public MangoPrice(boolean flag, String disnum, String number) {
		// TODO Auto-generated constructor stub
		this.flag = flag;
		this.disnum = disnum;
		this.number = number;
	}

	/*
	 * flag ：是否需要折扣
	 * disnum： 折扣数
	 * number：购买斤数*
	 */
	@Override
	public BigDecimal price() {
		// TODO Auto-generated method stub
		BigDecimal result = null;
		String  p =Fruit.MANGO.getPrice();
		BigDecimal b= new BigDecimal(p);
		BigDecimal num= new BigDecimal(number);
		
		if(flag) {
			BigDecimal disnumber= new BigDecimal(disnum);
		    result = (b.multiply(disnumber)).multiply(num);
		    System.out.println("草莓折后总价 --->"+result);
			return result;
		}
		
		result = b.multiply(num);
		
		System.out.println("芒果总价 --->"+result);
		return result;
	}

}
