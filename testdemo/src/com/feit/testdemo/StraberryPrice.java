package com.feit.testdemo;

import java.math.BigDecimal;

public class StraberryPrice implements FruitPrice {
	
	private String disnum;
	private String number;
	private boolean flag;

	public StraberryPrice(boolean flag, String disnum, String number) {
		// TODO Auto-generated constructor stub
		this.flag = flag;
		this.disnum = disnum;
		this.number = number;
	}

	/*
	 * flag ���Ƿ���Ҫ�ۿ�
	 * disnum�� �ۿ���
	 * number���������*
	 */
	@Override
	public BigDecimal price() {
		// TODO Auto-generated method stub
		BigDecimal result = null;
		String  p =Fruit.STRAWBERRY.getPrice();
		BigDecimal b= new BigDecimal(p);
		BigDecimal num= new BigDecimal(number);
		
		if(flag) {
			BigDecimal disnumber= new BigDecimal(disnum);
		    result = (b.multiply(disnumber)).multiply(num);
		    System.out.println("��ݮ�ۺ��ܼ� --->"+result);
			return result;
		}
		result = b.multiply(num);
		System.out.println("��ݮ�ܼ� --->"+result);
		return result;
	}

}
